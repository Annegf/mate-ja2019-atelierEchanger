# Installation
* Récupérer les sources
* Si les données ont été mises à jours, regénérer la page html grâce à la feuille XSLT. Par exemple :
  * sous Oxygen, en créant un scénario transformant `data/Post_it_tei.xml` avec `xslt/tei2html_postit.xsl` et avec la sortie `index.html`
  * en ligne de commande : `xsltproc xslt/tei2html_postit.xsl data/Post_it_tei.xml > index.html`
* Ouvrir la page index.html sur votre navigateur

# Autrices, auteurs et crédits
* L'atelier a été proposé et animé par Séverine Gedzelman <severine.gedzelman@ens-lyon.fr>, Justine Lascar <justine.lascar@ens-lyon.fr> et Anne Garcia-Fernandez <annegf@univ-grenoble-alpes.fr>. La restitution est le résultat d'un travail commun avec l'ensemble des participants à l'atelier qui s'est tenu grâce au Gliss (https://glisss.hypotheses.org/) qui a organisé les 6e journées annuelles du réseau Mate-SHS (https://glisss.hypotheses.org/ja2019 ; http://mate-shs.cnrs.fr/).
* Les photos ont été prises par Justine Lascar.
* Le site a été développé par Anne Garcia-Fernandez.

* Le code permettant de voir et animer des post-it est adapté du tuto de Chris Heilmann publié le 8 décembre 2011 sur https://code.tutsplus.com/tutorials/create-a-sticky-note-effect-in-5-easy-steps-with-css3-and-html5--net-13934