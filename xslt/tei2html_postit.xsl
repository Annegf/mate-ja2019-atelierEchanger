<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE tei2html_post-it_JA2019_atelierEchangerPartager [
    <!ENTITY times "&#215;">
    <!ENTITY non_breakable_space "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="1.0">
    <xsl:output method="html" indent="yes" omit-xml-declaration="yes"/>
    <xsl:template match="/">
        <html>
            <head>
                <meta charset="utf-8"/>
                <title>
                    <xsl:value-of
                        select="tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"/>
                </title>
                <link href="http://fonts.googleapis.com/css?family=Reenie+Beanie:regular"
                    rel="stylesheet" type="text/css"/>
                <link rel="stylesheet"
                    href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
                    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
                    crossorigin="anonymous"/>
                <link rel="stylesheet" href="css/atelierEchanger.css"/>
                <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"/>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"/>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"/>
            </head>
            <body id="header">
                <div class="container col col-12">
                    <nav class="navbar fixed-top navbar-light bg-light navbar-expand-lg">
                    <a class="navbar-brand" href="#">
                        <img alt="Réseau Méthodes Analyses Terrains Enquêtes en SHS"
                            src="http://mate-shs.cnrs.fr/IMG/siteon0.gif?1437396762"
                            title="Réseau Méthodes Analyses Terrains Enquêtes en SHS" height="30"
                            class="d-inline-block align-top"/>
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarText" aria-controls="navbarText" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"/>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarText">
                        <span class="navbar-text navbar-nav mr-auto mt-2 mt-lg-0">Réseau Méthodes Analyses Terrains
                        Enquêtes en SHS - Journées annuelles 2019 organisées par le Glisss,
                        Strasbourg</span>
                        <span class="navbar-text align-right">
                            <a href="https://gitlab.com/Annegf/mate-ja2019-atelierEchanger/">À
                                propos</a>
                        </span>
                    </div>
                </nav>
                </div>
                <div class="col mt-3 pt-3">
                    <img
                        href="https://f.hypotheses.org/wp-content/blogs.dir/3940/files/2019/05/Affiche-JAV1.pdf"/>

                    <xsl:call-template name="create_carousel">
                        <xsl:with-param name="position">left</xsl:with-param>
                    </xsl:call-template>
                    <xsl:apply-templates
                        select="tei:TEI/tei:teiHeader/tei:encodingDesc/tei:projectDesc"/>
                    <xsl:apply-templates/>
                    <a id="back-to-top" class="btn btn-light rounded" href="#header"
                        title="Retourner en haut de la page"> HAUT </a>
                </div>
            </body>
        </html>

    </xsl:template>

    <xsl:template match="tei:teiHeader"/>

    <xsl:template match="tei:head">
        <h1>
            <xsl:value-of select="."/>
        </h1>
    </xsl:template>

    <xsl:template match="tei:p">
        <p class="{@style}">
            <xsl:copy-of select="node()"/>
        </p>
    </xsl:template>
    <xsl:template match="tei:list[@type = 'feedback']">
        <xsl:apply-templates mode="postit"/>
    </xsl:template>

    <xsl:template match="tei:item" mode="postit">
        <div class="postit">
            <div class="postit-content" href="#postit">
                <xsl:apply-templates/>

            </div>
        </div>
    </xsl:template>

    <xsl:template match="tei:list">
        <ul>
            <xsl:apply-templates/>
        </ul>
    </xsl:template>

    <xsl:template match="tei:item">
        <li>
            <xsl:apply-templates/>
        </li>
    </xsl:template>

    <xsl:template name="create_carousel">
        <xsl:param name="position">right</xsl:param>
        <xsl:variable name="margin">
            <xsl:choose>
                <xsl:when test="$position = 'left'">
                    <xsl:text>r</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>l</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <div id="carouselExampleIndicators" class="carousel slide float-{$position} m{$margin}-3"
            data-ride="carousel">
            <ol class="carousel-indicators">
                <xsl:for-each select="//tei:figure">
                    <xsl:sort select="@n" order="ascending"/>
                    <li data-target="#carouselExampleIndicators">
                        <xsl:attribute name="data-slide-to">
                            <xsl:value-of select="position()"/>
                        </xsl:attribute>
                        <xsl:if test="position() = 1">
                            <xsl:attribute name="class">active</xsl:attribute>
                        </xsl:if>
                    </li>
                </xsl:for-each>
            </ol>
            <div class="carousel-inner">
                <xsl:attribute name="style">
                    <xsl:text>width: 300px;</xsl:text>
                </xsl:attribute>
                <xsl:for-each select="//tei:figure">
                    <xsl:sort select="@n" order="ascending"/>
                    <div>
                        <xsl:attribute name="class">
                            <xsl:text>carousel-item</xsl:text>
                            <xsl:if test="position() = 1"> active</xsl:if>
                        </xsl:attribute>
                        <img class="d-block rounded">
                            <xsl:attribute name="src">
                                <xsl:value-of select="@facs"/>
                            </xsl:attribute>
                            <xsl:attribute name="style">
                                <xsl:text>width: 300px;</xsl:text>
                            </xsl:attribute>
                        </img>
                    </div>
                </xsl:for-each>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"/>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"/>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </xsl:template>
</xsl:stylesheet>
